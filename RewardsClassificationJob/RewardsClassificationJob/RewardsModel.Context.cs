﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RewardsClassificationJob
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ApttusiCloudDBEntities : DbContext
    {
        public ApttusiCloudDBEntities()
            : base("name=ApttusiCloudDBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<BaseClassificationDetail> BaseClassificationDetails { get; set; }
        public virtual DbSet<BaseTransaction> BaseTransactions { get; set; }
        public virtual DbSet<ClassificationRule> ClassificationRules { get; set; }
    }
}
