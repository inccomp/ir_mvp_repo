﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.Reflection;
using System.Data.SqlClient;
using System.Linq.Expressions;

namespace RewardsClassificationJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    class RewardsClassification
    {
        static List<ClassificationRule> ClassificationRuleQuery;
        static List<BaseClassificationDetail> ClassificationList;
        static List<BaseTransaction> BaseTransactionList;
        // Please set the following connection strings in app.config for this WebJob to run:
        //// AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            var host = new JobHost();
            //// The following code ensures that the WebJob will be running continuously

            //Querying with LINQ to Entities 
            using (var context = new ApttusiCloudDBEntities())
            {
                BaseTransactionList = context.BaseTransactions.AsParallel().Where((t) => t.RecordStatus != null).ToList();
                ClassificationRuleQuery = context.ClassificationRules.AsParallel().Where((c) => c.ReferenceHierarchy != null).ToList();
                ClassificationList = context.BaseClassificationDetails.Where(c => c.hierarchyid != null).ToList();
            }

            var sync = new object();

            Parallel.ForEach<BaseTransaction>(BaseTransactionList, tf =>
            {
                
                List<ClassificationRule> ActiveClassificationHierarchyList = ClassificationRuleQuery.Where((s) => s.EffectiveStartDate < tf.sourceTransactionDate && s.EffectiveEndDate > tf.sourceTransactionDate).ToList();
                int Status = 0;
                String ClassificationStr = String.Empty;
                foreach (ClassificationRule ch in ActiveClassificationHierarchyList)
                {
                    PropertyInfo Prop = tf.GetType().GetProperties().FirstOrDefault(p => p.Name == ch.TransactionColumn);
                    if (Prop != null)
                    {
                        String attribute = (String)Prop.GetValue(tf);
                        if (attribute != null)
                        {
                            List<BaseClassificationDetail> ClassificationTree = (List<BaseClassificationDetail>)GetClassificationTree(attribute, tf.sourceTransactionDate);
                            if (ClassificationTree.Count > 1)
                                Status = 1;
                            foreach (BaseClassificationDetail cl in ClassificationTree)
                            {
                                if (ClassificationStr == String.Empty)
                                    ClassificationStr = cl.categoryname;
                                else
                                    ClassificationStr = ClassificationStr + "|" + cl.categoryname;
                            }

                        }
                    }
                }
                tf.Tags = ClassificationStr;
                tf.RecordStatus = (byte) Status;
            });


            using (var context = new ApttusiCloudDBEntities())
            {
                context.BulkUpdate(BaseTransactionList, operation => { operation.BatchSize = 5000;
                    operation.BatchTimeout = 500;
                    operation.DataTableBatchSize = 100000;
                    operation.AutoMapOutputDirection = false;
                    operation.SqlBulkCopyOptions = SqlBulkCopyOptions.TableLock;
                });
            }
            host.RunAndBlock();
        }


        private static IEnumerable<BaseClassificationDetail> GetClassificationTree(String TransactionField, DateTime? TranscationDate)
        {
            //Querying with Object Services and Entity SQL
            List<BaseClassificationDetail> ClassificationTree = ClassificationList.Where((c) => c.categoryname == TransactionField && c.EffectiveStartDate < TranscationDate && c.EffectiveEndDate > TranscationDate).ToList();
            if (ClassificationTree.Count > 0)
            {
                BaseClassificationDetail ClassificationBase = ClassificationTree.First();

                while (ClassificationBase != null)
                {
                    ClassificationBase = ClassificationList.FirstOrDefault((cl) => cl.categoryid == ClassificationBase.categoryparent && cl.EffectiveStartDate < TranscationDate && cl.EffectiveEndDate > TranscationDate);
                    if (ClassificationBase != null)
                        ClassificationTree.Add(ClassificationBase);
                }
            }
            return ClassificationTree;
        }


    }
}
