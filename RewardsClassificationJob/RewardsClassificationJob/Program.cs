﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.Reflection;
using System.Data.SqlClient;

namespace RewardsClassificationJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        static List<ClassificationRule> ClassificationMasterQuery;
        static List<BaseClassificationDetail> ClassificationList;
        static List<StageTransaction> StageTransactionList;
        // Please set the following connection strings in app.config for this WebJob to run:
        //// AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            var host = new JobHost();
            //// The following code ensures that the WebJob will be running continuously

            List<BaseTransaction> BaseTransactionList = new List<BaseTransaction>();
            List<StageTransaction> StageTransactionToInsertList = new List<StageTransaction>();
            //Querying with LINQ to Entities 
            using (var context = new ApttusiCloudDBEntities())
            {
                StageTransactionList = context.StageTransactions.AsParallel().Where((t) => t.RecordStatus != null).ToList();
                ClassificationMasterQuery = context.ClassificationRules.AsParallel().Where((c) => c.ReferenceHierarchy != null).ToList();
                ClassificationList = context.BaseClassificationDetails.Where(c => c.hierarchyid != null).ToList();
            }

            var sync = new object();

            Parallel.ForEach<StageTransaction>(StageTransactionList, tf =>
            {
                BaseTransaction bt = new BaseTransaction();
                bt.AlternateOrderNumber = tf.AlternateOrderNumber;
                bt.BatchName = tf.BatchName;
                bt.BusinessUnit = tf.BusinessUnit;
                bt.BusinessUnitName = tf.BusinessUnitName;
                bt.Channel = tf.Channel;
                bt.City = tf.City;
                bt.Comments = tf.Comments;
                bt.Country = tf.Country;
                bt.creditDate = tf.creditDate;
                bt.Customer = tf.Customer;
                bt.Discount = tf.Discount;
                bt.DiscountReason = tf.DiscountReason;
                bt.DiscountType = tf.DiscountType;
                bt.eventDate = tf.eventDate;
                bt.Hold = tf.Hold;
                bt.PaymentTerms = tf.PaymentTerms;
                bt.PONumber = tf.PONumber;
                bt.PostalCode = tf.PostalCode;
                bt.ProductDescription = tf.ProductDescription;
                bt.ProductId = tf.ProductId;
                bt.ProductName = tf.ProductName;
                bt.Province = tf.Province;
                bt.quantity = tf.quantity;
                bt.RecordStatus = tf.RecordStatus;
                bt.RollUpDate = tf.RollUpDate;
                bt.sourceTransactionDate = tf.sourceTransactionDate;
                bt.State = tf.State;
                bt.subLineNumber = tf.subLineNumber;
                bt.Tags = tf.Tags;
                bt.TransactionAmount = tf.TransactionAmount;
                bt.TransactionID = tf.TransactionID;
                bt.TransactionLineNumber = tf.TransactionLineNumber;
                bt.TransactionSource = tf.TransactionSource;
                bt.TransactionType = tf.TransactionType;
                bt.unitPrice = tf.unitPrice;
                bt.unitTypeValue = tf.unitTypeValue;
                bt.UpdatedBy = tf.UpdatedBy;
                bt.UpdatedDate = tf.UpdatedDate;

                List<ClassificationRule> ActiveClassificationHierarchyList = ClassificationMasterQuery.Where((s) => s.EffectiveStartDate < tf.sourceTransactionDate && s.EffectiveEndDate > tf.sourceTransactionDate).ToList();
                int Status = 0;
                String ClassificationStr = String.Empty;
                foreach (ClassificationRule ch in ActiveClassificationHierarchyList)
                {
                    PropertyInfo Prop = tf.GetType().GetProperties().FirstOrDefault(p => p.Name == ch.TransactionColumn);
                    if (Prop != null)
                    {
                        String attribute = (String)Prop.GetValue(tf);
                        if (attribute != null)
                        {
                            List<BaseClassificationDetail> ClassificationTree = (List<BaseClassificationDetail>)GetClassificationTree(attribute, tf.sourceTransactionDate);
                            if (ClassificationTree.Count > 1)
                                Status = 1;
                            foreach (BaseClassificationDetail cl in ClassificationTree)
                            {
                                if (ClassificationStr == String.Empty)
                                    ClassificationStr = cl.categoryname;
                                else
                                    ClassificationStr = ClassificationStr + "|" + cl.categoryname;
                            }

                        }
                    }
                }
                bt.Tags = ClassificationStr;
                tf.RecordStatus = (byte) Status;
                bt.RecordStatus = (byte) Status;              
                lock (sync)
                    BaseTransactionList.Add(bt);
            });

            using (var context = new ApttusiCloudDBEntities())
            {
                context.Database.CommandTimeout = 500;
                context.BulkInsert(BaseTransactionList, operation => { operation.BatchSize = 5000;
                    operation.BatchTimeout = 500;
                    operation.DataTableBatchSize = 100000;
                    operation.AutoMapOutputDirection = false;
                    operation.SqlBulkCopyOptions = SqlBulkCopyOptions.TableLock;
                });
            }

            BaseTransactionList.Clear();

            using (var context = new ApttusiCloudDBEntities())
            {
                context.BulkUpdate(StageTransactionList, operation => { operation.BatchSize = 5000;
                    operation.BatchTimeout = 500;
                    operation.DataTableBatchSize = 100000;
                    operation.AutoMapOutputDirection = false;
                    operation.SqlBulkCopyOptions = SqlBulkCopyOptions.TableLock;
                });
            }
            StageTransactionList.Clear();
            host.RunAndBlock();
        }


        private static IEnumerable<BaseClassificationDetail> GetClassificationTree(String TransactionField, DateTime? TranscationDate)
        {
            //Querying with Object Services and Entity SQL
            List<BaseClassificationDetail> ClassificationTree = ClassificationList.Where((c) => c.categoryname == TransactionField && c.EffectiveStartDate < TranscationDate && c.EffectiveEndDate > TranscationDate).ToList();
            if (ClassificationTree.Count > 0)
            {
                BaseClassificationDetail ClassificationBase = ClassificationTree.First();

                while (ClassificationBase != null)
                {
                    ClassificationBase = ClassificationList.FirstOrDefault((cl) => cl.categoryid == ClassificationBase.categoryparent && cl.EffectiveStartDate < TranscationDate && cl.EffectiveEndDate > TranscationDate);
                    if (ClassificationBase != null)
                        ClassificationTree.Add(ClassificationBase);
                }
            }
            return ClassificationTree;
        }
    }
}
